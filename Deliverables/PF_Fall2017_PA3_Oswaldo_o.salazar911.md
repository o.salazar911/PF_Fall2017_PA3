## **Programing Fundamentals - Project Assignment 3**

### **Course Code:**  SODV1101

### **Term / Year:** Fall 2017

### **Assignment code:** PA3

### **Author:** OSWALDO SALAZAR

### **BVC User Name:**  o.salazar911

### **Date Created:**  2017-10-17

### **Description:**  Submission File

**Repository Address:**

Latest versions of each document in the following link: 

https://gitlab.com/o.salazar911/PF_Fall2017_PA3.git

**External Resources Declaration:**

The only document used for the completion of this assignment was ***The Python Language Companion for Starting Out with Programming Logic and Design***, 4th Edition, By Tony Gaddis.

Other than the instructor, no other sources were utilized.

**Self Reflection:**

This assignment represented interesting challenges that provided to be fruitfull. I was able to connect my analysis thought process learned over the years in my Quality Assurance career, with the "new" approach that I am learning through the documentation of pseudocode, the fowcharting of a program, and coding / debugging in Python.

I started the assignment by flowcharting the problem stated and creating the hierarchy chart. The first draft of these charts provided a sequence to be addressed in the pseudocode, the next step.

Translating the pseudocode to python language was the most challenging part. The sequence initially considered viable turned out to be useless when trying to run the program. The following changes were required:

* The concept of modularizing the input statements - Turns out that it is easier and less complicated (meaning that syntax errors were eliminated) if these are part of the sequence of the main module.

* The discount calculation as a negative number - since the negative sign is for printing purposes, the actual sign was changed from being part of the variable, to being printed as a text before the variable is displayed. This also saved some syntax errors.

* The internal variables to calculate the tip - Initially I thought all variables needed to be declared in the main module. If used only within a module, these need not to be declared at the beginning, leaving a more cleaner code. This also was implemented in the module for the bill breakup.

Once these items were corrected, it was necessary to correct the charts, the pseudocode and the algorithm. Since the code ran as expected in Thonny, the review proved beneficial to ensure consistency among documents.

Testing was done with different values to ensure all tip options were working as expected. Also the non discount option was tested. The bills breakup was also shown to be flawless. I came up with the logic of this module by testing it first in excel.

Finally I would like to review the results of the pylint and codacy reports to understand better what they express. This will be set up with the instructor.





