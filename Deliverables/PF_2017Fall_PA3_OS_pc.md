## Programing Fundamentals - Project Assignment 3
__Course Code:__  SODV1101

__Term / Year:__ Fall 2017

__Assignment code:__ PA3

__Author:__ OSWALDO SALAZAR

__BVC User Name:__  o.salazar911

__Date Created:__  2017-10-15

__Description:__  Algorithm, IPO Chart and Pseudocode


**d. Algorithm:** 

The program asks the user to enter the "Meal Price" (variable *meal_price*, converted to a real number), and to enter a "Coupon Code", if there is one (variable *coupon_code*, maintained as a string).

If *coupon_code* is populated with the string "BVC", then the module  __"calc_discount"__ is executed to obtain the variable *discount*. The calculation is  *meal_price* times 0.1. In turn, if the user enters anything else, *discount* is set to zero. 

Next, the module __"calc_tax"__ is executed. The variable *tax* is calculated as 5 % of *meal_price* minus *discount*. 

Sequentialy, the module __"calc_tip"__ is executed. This module uses two internal variables: *meal_price_t* and *meal_price_t_d*. *meal_price_t* is used to determine the tip % base, and is the addition of the variables *tax* and *meal_price*. The variable *meal_price_t_d* is used to calculate the final tip as follows:

    The next sequence determines the value of *tip*:

    If *meal_price_t* is >= to 25.1, then *tip* is *meal_price_t_d* times 0.22 (or 22%);

    Otherwise, if *meal_price_t* is >= to 15.01, then *tip* is *meal_price_t_d* times 0.18 (or 18%);

    Otherwise, if *meal_price_t* is >= to 10.01, then *tip* is *meal_price_t_d* times 0.15 (or 15%);

    Otherwise, if *meal_price_t* is >= to 5, then *tip* is *meal_price_t_d* times 0.12 (or 12%);

    Otherwise, if *meal_price_t* is less than 4.99, then *tip* is *meal_price_t_d* times 0.1 (or 10%).

Next, the "Total" is calculated in the __"calc_total"__ module. The variable *total* is *meal_price* minus *discount*, plus *tip*, plus *tax*.

The module __"receipt"__ determines what is to be printed. If the variable *coupon_code* is equal to "BVC", then the following is displayed:

    Receipt:
    The meal price is $    *meal_price*
    Coupon code is         *coupon_code*
    The discount is $      *discount*
    The tax is $           *tax*
    The tip is $           *tip*
    The total is $         *total*

Otherwise, the following is displayed:

    Receipt:
    The meal price is $    *meal_price*
    The tax is $           *tax*
    The tip is $           *tip*
    The total is $         *total*

The final module calculates the bill breakdown to be printed on the receipt, based on the variable *total*. First __"bill_brake"__ sets the variables *bill_20*, *bill_10*, *bill_5*, and *bill_1* as integers, and to the value of zero. Then it calculates each actual value as follows:

    bill_20 = total / 20
    bill_10 = (total - (20 * bill_20)) / 10
    bill_5 = (total - (20 * bill_20) - (10 * bill_10)) / 5
    bill_1 = total - (20 * bill_20) - (10 * bill_10) - (5 * bill_5)

The decisions of which of these four variables is to be included in the receipt depends on their value. If they are not zero, then their value is displayed, along with its denomination. The variable that has a zero value is not displayed.  

**e. IPO Chart:**

| Input	| Process |  Output  |
| ----- | :------: |  ------  |
| User enters *meal_price*	| | |
| User enters *coupon_code* | Calculate 10% of *meal_price*, only if *coupon_code* is "BVC". Assign value to variable *discount* |
|| Calculate the variables: *tax* = (5% of *meal_price* - *discount*), *tip* = (22% or 18% or 15% or 12% or 10% of *meal_price* + *tax*), *total, bill_20, bill_10, bill_5, bill_1* | Display " Receipt: ", "The meal price is $:", *meal_price*, "Coupon code is", *coupon_code*, "The discount is $", *discount*, "The tax is $", *tax*, "The tip is $", *tip*, "The total is $", *total*, *bill_20*, "20's", *bill_10*, "10's", *bill_5*, "5's", *bill_1*, "1's" as applicable|

**g. Pseudocode:**

~~~

Module Main()
    // Variable Declaration
    Declare Real meal_price
    Declare String coupon_code
    Declare Real discount
    Declare Real tax
    Declare Real tip
    Declare Real total

    //User input Interface

    Display ("Please enter the Meal Price:")
    input meal_price
    Display ("Please enter the Coupon Code, if you have one:)
    input coupon_code

    // Definition if a discount is calculated

    If coupon_code is = to "BVC" then
        call calc_discount (meal_price)
    Else
        set discount = 0
    End If

    // Calling Processing Modules

    Set tax= calc_tax(meal_price, discount)
    Set tip= calc_tip(meal-price, tax, discount)
    Call calc_tax (meal_price, discount)
    Call calc_tip (meal_price_t)
    Set total = calc_total(meal_price, discount, tip, tax)
    Call receipt (meal_price, coupon_code, discount, tax, tip, total)
    Call bill_break (total)

End Module

Module get_coupon_code ()
    Display "Please enter the Coupon Code, if you have one: "
    Input coupon_code
End Module
 
// Tax calculation

Module calc_tax (meal_price, discount)
    Set tax = (meal_price - discount)* 0.05
End Module

// Tip calculation

Module calc_tip (meal_price, tax, discount)
    Set meal_price_t=(meal_price + tax)
    Set meal_price_t_d=(meal_price_t - discount)
    If meal_price_t >= 25.01 then
        Set tip = meal_price_t_d * 0.22
    Else If meal_price_t >= 15.01 then
        Set tip = meal_price_t_d * 0.18
    Else If meal_price_t_d >= 10.01 then
        Set tip = meal_price_t * 0.15
    Else If meal_price_t_d >= 5 then
        Set tip = meal_price_t_d * 0.12
    Else
        Set tip = meal_price_t_d * 0.1
    End If
End Module

// Discount Calculation

Module calc_discount (meal_price)
    Set discount = (meal_price * 0.1)
End Module

// Total Calculation

Module calc_total (meal_price, discount, tip, tax)
    Set total = meal_price - discount + tip + tax
End Module

// Receipt Information definition

Module receipt (meal_price, coupon_code, discount, tip, tax, total)
    If coupon_code = "BVC" then
        Display "Receipt:"
        Display "The meal price is $ ", meal_price
        Display "Coupon Code is ", coupon_code
        Display "The Discount is $ -", discount
        Display "The tax is $ ", tax
        Display "The tip is $ ", tip
        Display "The total is $, total
    Else
        Display "Receipt:"
        Display "The meal price is $ ", meal_price
        Display "The tax is $ ", tax
        Display "The tip is $ ", tip
        Display "The total is $, total
    End If
End Module

// Bill Brake Calculation

Module bill_brake (total)
    
    // Variables Initialization
    
    Set bill_20 = 0
    Set bill_10 = 0
    Set bill_5 = 0
    Set bill_1 = 0

    // Variables calculation based on total
    
    Set bill_20 = int(total / 20)
    Set bill_10 = int((total - (20 * bill_20)) / 10)
    Set bill_5 = int((total - (20 * bill_20) - (10 * bill_10)) / 5)
    Set bill_1 = int(total - (20 * bill_20) - (10 * bill_10) - (5 * bill_5))

    //Definition of what bill denomination is to be shown on the receipt
    
    If bill_20 > 0 then
        Display bill_20, " x 20s"
    Else If bill_10 > 0 then
        Display bill_10, " x 10s"
    Else If bill_5 > 0 then
        Display bill_5, " x 5s"
    Else If bill_1 > 0 then
        Display bill_1, " x 1s"
    End If
End Module

Call main()
~~~
