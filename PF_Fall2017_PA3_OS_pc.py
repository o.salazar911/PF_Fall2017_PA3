# Course Code: SODV1101
# Term / Year: Fall 2017
# Assignment code: PA3
# Author: OSWALDO SALAZAR
# BVC User Name:  o.salazar911
# Date Created:  2017-10-16
# Description: python code

# This program calculates the total value of a meal after
# applying an optional 10% discount, a 5% tax,and a % tip, along
# with the breakdown of total in as few bills as possible.

# Bills available are 20s, 10s, 5s, and 1s.

# The user enters the meal price and coupon code. The program
# then calculates discount, tax, tip, the total, and the total
# breakdown.

# Discount is only applied when a coupon code is provided.

# Tax is calculated after discount but before tip.

# The total is the meal price minus discount plus the tip, plus tax.

# Main module definition. Input modules are in the main module.


def main ():
    meal_price=float(input("Please enter the Meal Price: "))
    coupon_code=str(input("Please enter the Coupon Code, if you have one: "))

# Definition if a discount is calculated
    if coupon_code != "BVC":
        discount = 0
    else:
        discount= calc_discount(meal_price)

    tax=calc_tax(meal_price,discount)
    tip=calc_tip(meal_price,tax, discount)
    calc_tax(meal_price,discount)
    calc_tip(meal_price,tax,discount)
    total= calc_total(meal_price,discount,tip,tax)
    receipt(meal_price,coupon_code,discount,tax,tip,total)
    bill_brake (total)

# Tax calculation

def calc_tax (meal_price, discount):
    tax = 0.05 * (meal_price - discount)
    return tax


# Tip calculation

def calc_tip(meal_price, tax, discount):
    # Local Variables for the calculation of the tip
    # meal_price_t is required to determine the tip % base
    # meal_price_t_d is required to calculate the final tip
    meal_price_t=(meal_price + tax)
    meal_price_t_d=(meal_price_t - discount)
    if (meal_price_t) >= 25.01:
        tip = (meal_price_t_d) * 0.22
    elif (meal_price_t) >= 15.01:
        tip = (meal_price_t_d) * 0.18
    elif (meal_price_t) >= 10.01:
        tip = (meal_price_t_d) * 0.15
    elif (meal_price_t)>= 5:
        tip = (meal_price_t_d) * 0.12
    elif (meal_price_t) >0:
        tip = (meal_price_t_d) * 0.1
    return tip
        
# Discount Calculation

def calc_discount (meal_price):
    discount = meal_price * 0.1
    return discount
    
# Calculation of Total

def calc_total(meal_price, discount, tip, tax):
    total = meal_price - discount + tip + tax
    return total
    
   # Receipt Information definition

def receipt(meal_price, coupon_code, discount, tax, tip, total):
    if coupon_code == "BVC":
        print ("Receipt:")    # Variable definition
        print ("The meal price is $", meal_price)
        print ("Coupon Code is ", coupon_code)
        print ("The discount is $ -", discount)
        print ("The tax is $", format(tax, '.2f'))
        print ("The tip is $", format(tip, '.3f'))
        print ("The total is $", format(total, '.3f'))
    else:
        print ("Receipt:")
        print ("The meal price is $", meal_price)
        print ("The tax is $", format(tax, '.2f'))
        print ( "The tip is $", format(tip, '.3f'))
        print ("The total is $", format(total, '.3f'))

# Bill Brake Calculation

def bill_brake(total):

# Local Variables Initialization
    
    bill_20 = 0
    bill_10 = 0
    bill_5 = 0
    bill_1 = 0

# Local Variables calculation based on total
    
    bill_20 = int(total / 20)
    bill_10 = int((total - (20 * bill_20)) / 10)
    bill_5 = int((total - (20 * bill_20) - (10 * bill_10)) / 5)
    bill_1 = int(total - (20 * bill_20) - (10 * bill_10) - (5 * bill_5))

# Definition of what bill denomination is to be shown on the receipt
    
    if bill_20 > 0:
        print (bill_20, " x 20s")
    if bill_10 > 0:
        print (bill_10, " x 10s")
    if bill_5 > 0:
        print (bill_5, " x 5s")
    if bill_1 > 0:
        print (bill_1, " x 1s")

# Main module is called
main()

