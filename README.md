**Course code         :**   SODV1101

**Term/Year           :**   Fall2017

**Assignment code     :**   PA3

**Author              :**   Oswaldo Salazar

**BVC username        :**   o.salazar911

**Date created        :**   2017-10-13

**Description         :**   README file

Assignment to write a program to calculate the total value of a meal after
applying an optional 10% discount, a 5% tax,and a % tip, along with the
breakdown of total in as few bills as possible.

Bills available are 20s, 10s, 5s, and 1s.

The user will enter the meal price and coupon code. The program will then
calculate discount, tax, tip, the total, and the total breakdown.

Discount is only applied when a coupon code is provided.

Tax is calculated after discount but before tip.

The total is the meal price minus discount plus the tip, plus tax.
